package edu.cei.tct.cliente;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.common.servidor.Window;

public class ClienteImpl extends UnicastRemoteObject implements Cliente {

	private Window mainWindow;
	private Servidor servidor;
	private String id;

	public ClienteImpl(Window mainWindow) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry(1099);
		this.servidor = (Servidor) registry.lookup("server");
		this.mainWindow = mainWindow;
		this.id = String.valueOf(System.currentTimeMillis());
		this.servidor.registrarCliente(this, id);
	}

	@Override
	public void recibirMensaje(String mensaje) {
		this.mainWindow.mostrarMensaje(mensaje);
	}

	@Override
	public void enviarMensaje(String mensaje) throws RemoteException {
		this.servidor.hola(mensaje, this.id);
	}
}
