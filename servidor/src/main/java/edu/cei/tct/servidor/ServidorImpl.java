package edu.cei.tct.servidor;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Servidor;

public class ServidorImpl implements Servidor {

	private Map<String, Cliente> clientes = new HashMap<>();

	@Override
	public void registrarCliente(Cliente cliente, String id) throws RuntimeException {
		this.clientes.put(id, cliente);
	}

	@Override
	public String hola(String nombre, String id) throws RemoteException {
		// String mensaje = "Hola " + nombre + " desde el otro lado del cable";
		StringBuilder sb = new StringBuilder();
		sb.append("Hola ");
		sb.append(nombre);
		sb.append(" desde el otro lado del cable");
		String mensaje = sb.toString();
		System.out.println(mensaje);

		for(String key : clientes.keySet()) {
			//key != id
			if(!key.equals(id)) {
				clientes.get(key).recibirMensaje(mensaje);
			}
		}

		return mensaje;
	}
}
