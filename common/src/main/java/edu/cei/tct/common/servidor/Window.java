package edu.cei.tct.common.servidor;

import java.io.Serializable;

public interface Window extends Serializable {

	void mostrarMensaje(String mensaje);

}
