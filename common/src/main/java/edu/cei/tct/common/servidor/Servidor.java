package edu.cei.tct.common.servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Servidor extends Remote {
	public String hola(String mensaje, String id) throws RemoteException;
	public void registrarCliente(Cliente cliente, String id) throws RemoteException;
}
