package edu.cei.tct.common.servidor;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Cliente extends Remote, Serializable {

	public void recibirMensaje(String mensaje) throws RemoteException;
	
	public void enviarMensaje(String mensaje) throws RemoteException;
}
